package tech.kevin.kevinandroidutilslib.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by kevin on 11/24/17.
 * Size convert util
 */

public class SizeUtils {
    /**
     * dp -> px
     */
    public static int dp2px(Context context, float dpValue) {
        final float currentDensity = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * currentDensity + 0.5f);
    }

    /**
     * px -> dp
     */
    public static int px2dp(Context context, float dxValue) {
        final float currentDensity = context.getResources().getDisplayMetrics().density;
        return (int) (dxValue / currentDensity + 0.5f);
    }

    /**
     * sp -> px
     */
    public static int sp2px(Context context, float spValue) {
        final float currentScaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * currentScaledDensity + 0.5);
    }

    /**
     * px -> sp
     */
    public static int px2sp(Context context, float pxValue) {
        final float currentScaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / currentScaledDensity + 0.5);
    }


    /**
     * Copy from TypedValue class.
     * This function is for value transform.
     *
     * @param unit    px,sp,dp
     * @param value   The value we want to transform.
     * @param metrics Context metric
     * @return Transformed result
     */
    public static float applyDimension(int unit, float value, DisplayMetrics metrics) {
        switch (unit) {
            case TypedValue.COMPLEX_UNIT_PX:
                return value;
            case TypedValue.COMPLEX_UNIT_DIP:
                return value * metrics.density;
            case TypedValue.COMPLEX_UNIT_SP:
                return value * metrics.scaledDensity;
            case TypedValue.COMPLEX_UNIT_PT:
                return value * metrics.xdpi * (1.0f / 72);
            case TypedValue.COMPLEX_UNIT_IN:
                return value * metrics.xdpi;
            case TypedValue.COMPLEX_UNIT_MM:
                return value * metrics.xdpi * (1.0f / 25.4f);
        }
        return 0;
    }

    /**
     * @param view The view needs to be measured. We always use this function in View's onCreate.
     * @return Array[0] is Width, Array[1] is height.
     */
    public static int[] getViewMeasure(View view) {
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(widthMeasureSpec, heightMeasureSpec);
        return new int[]{view.getMeasuredWidth(), view.getMeasuredHeight()};
    }


}
