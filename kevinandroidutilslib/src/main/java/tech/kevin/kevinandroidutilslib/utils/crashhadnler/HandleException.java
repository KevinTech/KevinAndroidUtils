package tech.kevin.kevinandroidutilslib.utils.crashhadnler;

/**
 * Data: 2017/11/23
 * Author: Kevin.Chiu
 * Description: Use with CrashHandler
 */

public interface HandleException {
    void handle();
}
