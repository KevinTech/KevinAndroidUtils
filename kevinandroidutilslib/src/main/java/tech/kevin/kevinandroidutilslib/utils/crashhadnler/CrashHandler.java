package tech.kevin.kevinandroidutilslib.utils.crashhadnler;

import android.util.Log;

/**
 * Data: 2017/11/23
 * Author: Kevin.Chiu
 * Description: A handler for application crash.
 */

public class CrashHandler implements Thread.UncaughtExceptionHandler {

    private static CrashHandler instance;
    private static final String TAG = "CrashHandler";
    private HandleException mHandleException = null;

    /**
     * Single instance, also get get the application context.
     */
    private CrashHandler() {
        super();
        Thread.setDefaultUncaughtExceptionHandler(this);
        Log.d(TAG, "CrashHandler: init");
    }

    /**
     * Handle uncaught exception here to make sure the application can work well forever.
     *
     * @param t Thread
     * @param e Throwable
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        Log.d(TAG, "uncaughtException: Thread");
        Log.d(TAG, "uncaughtException: Throwable");
        if (mHandleException != null) {
            Log.d(TAG, "uncaughtException: Begin handling");
            mHandleException.handle();
        }
    }

    /**
     * @return Single instance
     */
    public static synchronized CrashHandler getInstance() {
        if (instance == null) {
            instance = new CrashHandler();
        }
        return instance;
    }

    /**
     * Handle exceptions
     *
     * @param mHandleException HandleException
     */
    public void setHandleException(HandleException mHandleException) {
        this.mHandleException = mHandleException;
    }
}
