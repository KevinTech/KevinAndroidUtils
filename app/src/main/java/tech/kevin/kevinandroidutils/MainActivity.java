package tech.kevin.kevinandroidutils;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import tech.kevin.kevinandroidutilslib.utils.SizeUtils;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    @BindView(R.id.cl_main)
    ConstraintLayout clMain;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.btn)
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        int[] clMainViewMeasure = SizeUtils.getViewMeasure(clMain);
        Log.d(TAG, "onCreate: clMainViewMeasure width=" + clMainViewMeasure[0] + "--- height=" + clMainViewMeasure[1]);


        int[] tvViewMeasure = SizeUtils.getViewMeasure(tv);
        Log.d(TAG, "onCreate: tvViewMeasure width=" + tvViewMeasure[0] + "--- height=" + tvViewMeasure[1]);

        int[] btnViewMeasure = SizeUtils.getViewMeasure(btn);
        Log.d(TAG, "onCreate: btnViewMeasure width=" + btnViewMeasure[0] + "--- height=" + btnViewMeasure[1]);
    }
}
