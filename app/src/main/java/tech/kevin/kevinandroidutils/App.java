package tech.kevin.kevinandroidutils;

import android.app.Application;

import tech.kevin.kevinandroidutilslib.utils.crashhadnler.CrashHandler;
import tech.kevin.kevinandroidutilslib.utils.crashhadnler.HandleException;

/**
 * Created by Kobe on 2017/11/23.
 * Demo application
 */

public class App extends Application implements HandleException {
    @Override
    public void onCreate() {
        super.onCreate();
        CrashHandler ch = CrashHandler.getInstance();
        ch.setHandleException(this);
    }

    @Override
    public void handle() {

    }
}
